<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\Site;

class SiteController extends ApiController {
	public $defaultAction = 'list';
	protected $_except_action_check_domain = ['create', 'list', 'is-set', 'view'];
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new Site();

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}
			return $this->sendSuccess([
				'site' => $model->getAttributes()
			]);
		} else {
			$errors = $this->getErrorCodes([
				'name' => self::ERROR_ILLEGAL_SITE_NAME,
				'domain' => self::ERROR_ILLEGAL_SITE_DOMAIN,
			], $model);
			return $this->sendError($errors);
		}
	}
	public function actionUpdate() {
		if(\Yii::$app->request->isPost) {
			if (!$this->site->load(\Yii::$app->request->post())) {
				return $this->sendError(self::ERROR_NO_DATA);
			}

			if ($this->site->validate()) {
				if (!$this->site->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
			} else {
				$errors = $this->getErrorCodes([
					'name' => self::ERROR_ILLEGAL_SITE_NAME,
					'domain' => self::ERROR_ILLEGAL_SITE_DOMAIN,
				], $this->site);
				return $this->sendError($errors);
			}
		}

		return $this->sendSuccess([
			'site' => $this->site->getAttributes()
		]);
	}
	public function actionView($id) {
		if($model = Site::findOne(['id' => $id])) {
			/* @var Site $model */
			return $this->sendSuccess([
				'site' => $model->getAttributes()
			]);
		}
		return $this->sendError(self::ERROR_NO_SITE);
	}
	public function actionIsSet($id) {
		if(Site::findOne(['id' => $id])) {
			return $this->sendSuccess([]);
		}
		return $this->sendError(self::ERROR_NO_SITE);
	}
	public function actionDelete() {
		if($this->site->delete() === false) {
			return $this->sendError(self::ERROR_DB);
		} else {
			return $this->sendSuccess([]);
		}
	}
    public function actionList() {
	    $res = Site::find()
		    ->select('id, name, domain')
		    ->asArray()
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess(['sites' => $res]);
    }
}
