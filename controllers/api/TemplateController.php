<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\Template;

class TemplateController extends ApiController {
	public $defaultAction = 'list';
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new Template();

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}
		$model->site_id = $this->site->id;

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}
			return $this->sendSuccess(['template' => $model->getAttributes()]);
		} else {
			$errors = $this->getErrorCodes([
				'nick' => self::ERROR_ILLEGAL_TEMPLATE_NAME,
				'head' => self::ERROR_ILLEGAL_TEMPLATE_HEAD,
				'body_start' => self::ERROR_ILLEGAL_TEMPLATE_BODY_START,
				'body_end' => self::ERROR_ILLEGAL_TEMPLATE_BODY_END,
			], $model);
			return $this->sendError($errors);
		}
	}
	public function actionUpdate($id) {
		/* @var Template $model */
		$model = Template::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->one();
		if(!$model) {
			return $this->sendError(self::ERROR_NO_TEMPLATE);
		}
		if(\Yii::$app->request->isPost) {
			if (!$model->load(\Yii::$app->request->post())) {
				return $this->sendError(self::ERROR_NO_DATA);
			}

			if ($model->validate()) {
				if (!$model->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
			} else {
				$errors = $this->getErrorCodes([
					'nick' => self::ERROR_ILLEGAL_TEMPLATE_NAME,
					'head' => self::ERROR_ILLEGAL_TEMPLATE_HEAD,
					'body_start' => self::ERROR_ILLEGAL_TEMPLATE_BODY_START,
					'body_end' => self::ERROR_ILLEGAL_TEMPLATE_BODY_END,
				], $model);
				return $this->sendError($errors);
			}
		}

		return $this->sendSuccess(['template' => $model->getAttributes()]);
	}
	public function actionDelete($id) {
		/* @var Template $post */
		$page = Template::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->one();
		if($page) {
			if($page->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_POST);
		}
	}
    public function actionList() {
	    $res = Template::find()
		    ->select('id, name')
		    ->where(['site_id' => $this->site->id])
		    ->asArray()
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess([
		    'templates' => $res,
	    ]);
    }

}
