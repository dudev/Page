<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text
 * @property integer $template_id
 * @property integer $site_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Template $template
 * @property Site $site
 */
class Page extends \yii\db\ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nick', 'title', 'text', 'site_id'], 'required'],
            ['text', 'string'],
            ['template_id', 'integer'],
            [['nick', 'title'], 'string', 'max' => 100],
            [['keywords'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 250],
	        ['template_id', 'filter', 'filter' => function($value) {
		        if(!$value) {
			        return null;
		        }
		        return $value;
	        }],
	        [
		        'template_id',
		        'exist',
		        'targetClass' => Template::className(),
		        'targetAttribute' => [
			        'template_id' => 'id',
			        'site_id' => 'site_id'
		        ]
	        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick' => 'Nick',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'text' => 'Text',
            'template_id' => 'Template ID',
            'site_id' => 'Site ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
