<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $name
 * @property string $head
 * @property string $body_start
 * @property string $body_end
 * @property integer $site_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Page[] $pages
 */
class Template extends \yii\db\ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'site_id'], 'required'],
            [['head', 'body_start', 'body_end'], 'string'],
            ['site_id', 'integer'],
	        ['site_id', 'exist', 'targetClass' => Site::className(), 'targetAttribute' => 'id'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'head' => 'Head',
            'body_start' => 'Body Start',
            'body_end' => 'Body End',
            'site_id' => 'Site ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['template_id' => 'id']);
    }
}
