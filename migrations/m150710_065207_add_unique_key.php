<?php

use yii\db\Schema;
use yii\db\Migration;

class m150710_065207_add_unique_key extends Migration
{
    public function up()
    {
	    $this->createIndex('page_tbl_nick_site_id_idx', 'page', ['nick', 'site_id'], true);
	    $this->createIndex('template_tbl_name_site_id_idx', 'template', ['name', 'site_id'], true);
	    $this->createIndex('site_tbl_domain_idx', 'site', 'domain', true);
    }

    public function down()
    {
        echo "m150710_065207_add_unique_key cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
