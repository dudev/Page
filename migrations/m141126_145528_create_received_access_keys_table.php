<?php

use yii\db\Schema;
use yii\db\Migration;

class m141126_145528_create_received_access_keys_table extends Migration
{
    public function up()
    {
	    $this->createTable('received_access_keys', [
		    'access_key' => 'CHAR(64) PRIMARY KEY',
		    'service' => 'CHAR(20) NOT NULL',
		    'expires_in' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
	    ]);
	    $this->createIndex('received_access_keys_tbl_service_idx', 'received_access_keys', 'service', true);
    }

    public function down()
    {
        echo "m141126_145528_create_received_access_keys_table cannot be reverted.\n";

        return false;
    }
}
