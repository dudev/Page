<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;


use app\models\Site;
use yii\web\NotFoundHttpException;

/**
 * Class Controller
 * @property Site $site
 */
class Controller extends \yii\web\Controller {
	/* @var Site $_site */
	private  $_site;

	/**
	 * @param \yii\base\Action $action
	 * @return bool
	 * @throws NotFoundHttpException
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action) {
		if(parent::beforeAction($action)) {
			if(strpos(\Yii::$app->request->getServerName(), '.') === false) {
				throw new NotFoundHttpException();
			}
			if(!$this->getSite()) {
				throw new NotFoundHttpException();
			}
			return true;
		}
		return false;
	}

	/**
	 * @return Site
	 */
	public function getSite() {
		if($this->_site) {
			return $this->_site;
		}
		return $this->_site = Site::findOne(['domain' => \Yii::$app->request->getServerName()]);
	}
} 